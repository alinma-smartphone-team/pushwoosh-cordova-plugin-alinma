package com.pushwoosh.plugin;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.pushwoosh.internal.event.EventBus;
import com.pushwoosh.internal.event.EventListener;
import com.pushwoosh.internal.utils.PWLog;
import com.pushwoosh.plugin.geolocation.Callback;
import com.pushwoosh.plugin.geolocation.LocationManager;
import com.pushwoosh.plugin.geolocation.LocationSettingsResolutionActivity;
import com.pushwoosh.plugin.geolocation.PushGeolocationPlugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PushGeolocation extends CordovaPlugin {
    private LocationManager locationManager = new LocationManager();

    @SuppressLint("StaticFieldLeak")
    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackId) {
        if (action.equals("requestPermissions")) {
            locationManager.requestPermissions(cordova.getContext().getApplicationContext(), new Callback<Boolean>() {
                @Override
                public void call(Boolean result, Exception e) {
                    if (e != null)
                        PWLog.error("cannot receive permissions", e);
                }
            });
            return true;
        } else if (action.equals("startTrackingPushGeolocation")) {
            locationManager.requestPermissions(cordova.getContext().getApplicationContext(), new Callback<Boolean>() {
                @Override
                public void call(Boolean result, Exception e) {
                    if (e == null) {
                        PushGeolocationPlugin.sharedInstance().getStorage().setLocationTrackingEnbaled(true);
                        PushGeolocationPlugin.sharedInstance().setupHandler();
                        callbackId.success();
                    } else {
                        callbackId.error(errorToJSON(e, "Permission request failed"));
                    }
                }
            });
            return true;
        } else if (action.equals("stopTrackingPushGeolocation")) {
            PushGeolocationPlugin.sharedInstance().getStorage().setLocationTrackingEnbaled(false);
            PushGeolocationPlugin.sharedInstance().getStorage().clear();
            PushGeolocationPlugin.sharedInstance().removeHandler();
            return true;
        } else if (action.equals("popReceivedPushes")) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    final JSONArray result = PushGeolocationPlugin.sharedInstance().getStorage().pop();
                    callbackId.success(result);
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            return true;
        }

        return false;
    }

    private JSONObject errorToJSON(Exception error, String errorLogMessage) {
        PWLog.error("PushwooshSecurePlugin", errorLogMessage, error);
        JSONObject exceptionObject = new JSONObject();
        try {
            exceptionObject.put("error", error);
            exceptionObject.put("errorDescription", error.getLocalizedMessage());
        } catch (JSONException ignore) {
        }
        return exceptionObject;
    }
}
