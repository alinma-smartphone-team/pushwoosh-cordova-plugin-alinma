//
//  NotificationService.m
//  SecureExtension
//

#import "NotificationService.h"
#import "PWPushInterceptorProcessor.h"
#import "PWSSecureStorage.h"
#import <CoreLocation/CoreLocation.h>

#define kGeoPushTrackingKey @"GeoPushTracking"
#define kMaxPushSaved 20

@interface NotificationService () <CLLocationManagerDelegate>

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNNotificationContent *bestAttemptContent;

@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) BOOL needSaveLocation;
@property (nonatomic) BOOL decryptionFinished;
@property (nonatomic) CLLocation *pushLocation;

@property (nonatomic) NSArray *savedPushes;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content copy];
    
    PWPushInterceptorProcessor *processor = [PWPushInterceptorProcessor rsaAndBroadcastPushInterceptorProcessor];
    
    [processor processContent:request.content completion:^(UNNotificationContent *content, NSError *error) {
        if (error != nil) {
            NSLog(@"Error while processing notification content: %@", error.localizedDescription);
        }
        if (content) {
            if ([[content.userInfo objectForKey:@"trlc"] isEqual:@1]) {
                _savedPushes = (NSArray *)[PWSSecureStorage loadObjectWithTag:kGeoPushTrackingKey error:NULL];
                if (_savedPushes) {
                    _needSaveLocation = YES;
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        self.locationManager = [CLLocationManager new];
                        self.locationManager.delegate = self;
                        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                        [self.locationManager requestLocation];
                    }];
                }
            }
            self.bestAttemptContent = content;
        }
        if (!self.needSaveLocation) {
            self.contentHandler(self.bestAttemptContent);
        } else {
            @synchronized (self) {
                self.decryptionFinished = YES;
                if (self.pushLocation != nil) {
                    [self locationObtainedAndPushDecrypted];
                }
            }
        }
    }];
}

- (void)locationObtainedAndPushDecrypted {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    data[@"payload"] =  [self.bestAttemptContent.userInfo mutableCopy];
    data[@"geolocation"] = @{
                                    @"latitude" : @(_pushLocation.coordinate.latitude),
                                    @"longitude" : @(_pushLocation.coordinate.longitude),
                                    @"accuracy" : @(_pushLocation.horizontalAccuracy)
                                    };
    NSMutableArray *mutableSavedPushes = [_savedPushes mutableCopy];
    [mutableSavedPushes addObject:data];
    if (mutableSavedPushes.count > kMaxPushSaved) {
        [mutableSavedPushes removeObjectsInRange:NSMakeRange(0, mutableSavedPushes.count - kMaxPushSaved)];
    }
    [PWSSecureStorage saveObject:mutableSavedPushes.copy tag:kGeoPushTrackingKey error:NULL];
    self.locationManager = nil;
    self.contentHandler(self.bestAttemptContent);
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations {
    @synchronized (self) {
        _pushLocation = locations.lastObject;
        if (_decryptionFinished) {
            [self locationObtainedAndPushDecrypted];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    @synchronized (self) {
        if (_decryptionFinished) {
            self.contentHandler(self.bestAttemptContent);
        }
    }
}

- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    self.contentHandler(self.bestAttemptContent);
}

@end
