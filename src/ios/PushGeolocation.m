#import "PushGeolocation.h"
#import "PWLog.h"
#import "PWSSecureStorage.h"
#import <CoreLocation/CoreLocation.h>

#define kGeoPushTrackingKey @"GeoPushTracking"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@interface PushGeolocation () <CLLocationManagerDelegate>

@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CDVInvokedUrlCommand *startLocationCommand;

@end

@implementation PushGeolocation

#pragma clang diagnostic pop

- (void)requestPermissions:(CDVInvokedUrlCommand *)command {
    [[CLLocationManager new] requestAlwaysAuthorization];
}

- (void)startTrackingPushGeolocation:(CDVInvokedUrlCommand *)command {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            _startLocationCommand = command;
            _locationManager = [CLLocationManager new];
            _locationManager.delegate = self;
            [_locationManager requestAlwaysAuthorization];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [[CLLocationManager new] requestAlwaysAuthorization];
        case kCLAuthorizationStatusAuthorizedAlways:
            [self locationPermissionsGranted:command];
            break;
        default:
            [self sendMethod:command error:[NSError errorWithDomain:@"PushGeolocation" code:status userInfo:@{ NSLocalizedDescriptionKey : [NSString stringWithFormat:@"Cannot start track location. CLAuthorizationStatus: %ld", (long)status] }]];
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusNotDetermined) {
        return;
    }
    [self startTrackingPushGeolocation:_startLocationCommand];
    _locationManager.delegate = nil;
    _locationManager = nil;
    _startLocationCommand = nil;
}

- (void)sendMethod:(CDVInvokedUrlCommand *)command error:(NSError *)error {
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:@{ @"error" : error.description, @"errorDescription" : error.localizedDescription }] callbackId:command.callbackId];
}

- (void)sendMethodSuccess:(CDVInvokedUrlCommand *)command {
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:nil] callbackId:command.callbackId];
}

- (void)sendMethodSuccess:(CDVInvokedUrlCommand *)command resultArray:(NSArray *)result {
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:result] callbackId:command.callbackId];
}

- (void)locationPermissionsGranted:(CDVInvokedUrlCommand *)command {
    NSError *error = nil;
    [PWSSecureStorage saveObject:@[] tag:kGeoPushTrackingKey error:&error];
    if (error != nil) {
        PWLogError(@"%@ save failed with error: %@", kGeoPushTrackingKey, error);
        [self sendMethod:command error:error];
    } else {
        [self sendMethodSuccess:command];
    }
}

- (void)stopTrackingPushGeolocation:(CDVInvokedUrlCommand *)command {
    [PWSSecureStorage deleteObjectWithTag:kGeoPushTrackingKey error:NULL];
}

- (void)popReceivedPushes:(CDVInvokedUrlCommand *)command {
    NSError *error = nil;
    NSArray *pushes = (NSArray *)[PWSSecureStorage loadObjectWithTag:kGeoPushTrackingKey error:&error];
    if (error != nil) {
        PWLogError(@"%@ load failed with error: %@", kGeoPushTrackingKey, error);
        [self sendMethod:command error:error];
    } else {
        [PWSSecureStorage saveObject:@[] tag:kGeoPushTrackingKey error:NULL];
        [self sendMethodSuccess:command resultArray:pushes];
    }
    
}

@end
