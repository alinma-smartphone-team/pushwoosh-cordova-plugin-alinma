//
//  PWPushInterceptorProcessor.h
//  Secure Push
//  (c) Pushwoosh 2018
//

#import <Foundation/Foundation.h>
#import "PWPushInterceptor.h"
#import <UserNotifications/UserNotifications.h>

@interface PWPushInterceptorProcessor : NSObject

/**
 *  @brief Convenience initializer.
 *
 *  @details Automatically fills interceptors list with an array of objects conforming to @c PWPushInterceptor protocol.
 *
 *  @return Instance of @c PWPushInterceptorProcessor.
 */
- (instancetype)initWithInterceptors:(NSArray<id<PWPushInterceptor>> *)interceptors;

/**
 *  @brief Getter to access the list of registered interceptors.
 *
 *  @return List of objects conforming to @c PWPushInterceptor protocol, registered to instance of @c PWPushInterceptorProcessor.
 */
- (NSArray<id<PWPushInterceptor>> *)interceptors;

/**
 *  @brief Allows to register objects conforming to @c PWPushInterceptor protocol to instance of @c PWPushInterceptorProcessor.
 */
- (void)addInterceptors:(NSArray<id<PWPushInterceptor>> *)interceptors;

/**
 *  @brief Allows to deregister objects conforming to @c PWPushInterceptor protocol to instance of @c PWPushInterceptorProcessor.
 */
- (void)removeInterceptors:(NSArray<id<PWPushInterceptor>> *)interceptors;

/**
 *  @brief Process content and returns modified version.
 */
- (void)processContent:(UNNotificationContent *)content completion:(void(^)(UNNotificationContent *content, NSError *error))completion;

/**
 *  @brief Process userInfo and returns modified version.
 */
- (void)processUserInfo:(NSDictionary *)userInfo completion:(void(^)(NSDictionary *payload, NSError *error))completion;

@end

@interface PWPushInterceptorProcessor (RSA)

/**
 *  @brief Ready to use RSA based implementation.
 *
 *  @return Instance of @c PWPushInterceptorProcessor.
 */
+ (instancetype)rsaPushInterceptorProcessor;

@end

@interface PWPushInterceptorProcessor (RSAAndBroadcast)

/**
 *  @brief Ready to use RSA based implementation.
 *
 *  @return Instance of @c PWPushInterceptorProcessor.
 */
+ (instancetype)rsaAndBroadcastPushInterceptorProcessor;

@end
