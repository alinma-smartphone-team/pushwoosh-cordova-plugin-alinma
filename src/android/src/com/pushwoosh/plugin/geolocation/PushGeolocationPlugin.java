package com.pushwoosh.plugin.geolocation;

import com.pushwoosh.internal.Plugin;
import com.pushwoosh.internal.platform.prefs.PrefsProvider;
import com.pushwoosh.internal.platform.prefs.migration.MigrationScheme;
import com.pushwoosh.notification.handlers.message.system.MessageSystemHandleChainProvider;
import com.pushwoosh.notification.handlers.message.system.MessageSystemHandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class PushGeolocationPlugin implements Plugin {
    private LocationPushesStorage storage;
    private static PushGeolocationPlugin sharedInstance = null;
    @Override
    public void init() {
        sharedInstance = this;
        storage = new LocationPushesStorage();
        if (storage.isLocationTrackingEnabled()) {
            setupHandler();
        }
    }

    public void setupHandler() {
        removeHandler();

        MessageSystemHandleChainProvider.getMessageSystemChain().addItem(new LocationMessageHandler(storage));
    }

    public void removeHandler() {
        Iterator<MessageSystemHandler> it = MessageSystemHandleChainProvider.getMessageSystemChain().getIterator();

        ArrayList<MessageSystemHandler> handlersToRemove = new ArrayList<>();

        while(it.hasNext()) {
            MessageSystemHandler handler = it.next();
            if (handler instanceof LocationMessageHandler) {
                handlersToRemove.add(handler);
            }
        }

        for (MessageSystemHandler h : handlersToRemove) {
            MessageSystemHandleChainProvider.getMessageSystemChain().removeItem(h);
        }
    }

    public LocationPushesStorage getStorage() {
        return storage;
    }

    public static PushGeolocationPlugin sharedInstance() {
        return sharedInstance;
    }

    @Override
    public Collection<? extends MigrationScheme> getPrefsMigrationSchemes(PrefsProvider prefsProvider) {
        return null;
    }
}
