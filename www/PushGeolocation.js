//
//  PushNotification.js
//
// Based on the Push Notifications Cordova Plugin by Olivier Louvignes.
// Modified by Pushwoosh team.
//
// Pushwoosh Push Notifications Plugin for Cordova
// www.pushwoosh.com
//
// MIT Licensed

var exec = require('cordova/exec');

//Class: PushGeolocation
function PushGeolocation() {}

PushGeolocation.prototype.requestPermissions = function() {
	exec(nil, nil, "PushGeolocation", "requestPermissions", []);
};

PushGeolocation.prototype.startTrackingPushGeolocation = function(success, fail) {
	exec(success, fail, "PushGeolocation", "startTrackingPushGeolocation", []);
};

PushGeolocation.prototype.stopTrackingPushGeolocation = function() {
	exec(nil, nil, "PushGeolocation", "stopTrackingPushGeolocation", []);
};

PushGeolocation.prototype.popReceivedPushes = function(success, fail) {
	exec(success, fail, "PushGeolocation", "popReceivedPushes", []);
};

module.exports = new PushGeolocation();