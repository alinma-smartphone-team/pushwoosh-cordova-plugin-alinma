//
// PushNotifications.java
//
// Pushwoosh, 01/07/12.
//
// Pushwoosh Push Notifications Plugin for Cordova Android
// www.pushwoosh.com
//
// MIT Licensed

package com.pushwoosh.plugin;

import android.support.annotation.NonNull;

import com.pushwoosh.exception.PushwooshException;
import com.pushwoosh.function.Callback;
import com.pushwoosh.function.Result;
import com.pushwoosh.internal.utils.PWLog;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PushwooshSecure extends CordovaPlugin {
    
    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackId)
    {
        try {
            if (action.equals("setBaseURL")) {
                if (data.length() == 0) {
                    PWLog.error("PushwooshSecure", "setBaseURL: not enough params");
                    return true;
                }
                JSONObject params = data.getJSONObject(0);

                String apiUrl = params.getString("baseURL");
                JSONArray pinsJSONArray = params.optJSONArray("publicKeyPins");
                String overrideHost = params.optString("overrideHost", null);

                ArrayList<String> pins = new ArrayList<>();
                for (int i = 0; i < pinsJSONArray.length(); ++i) {
                    Object obj = pinsJSONArray.get(i);
                    if (obj instanceof String) {
                        pins.add((String)obj);
                    } else {
                        PWLog.error("PushwooshSecure", "pin should be base64 string");
                    }
                }

                com.pushwoosh.secure.PushwooshSecure.setBaseURL(apiUrl, pins, overrideHost);
                return true;
            } else if (action.equals("setupDecryption")) {
                com.pushwoosh.secure.PushwooshSecure.setupDecryption(wrapCallback(callbackId, "Error while setuping secure pushes"));
                return true;
            } else if (action.equals("teardownDecryption")) {
                com.pushwoosh.secure.PushwooshSecure.teardownDecryption(wrapCallback(callbackId, "Error while tearing down secure pushes"));
                return true;
            }
        } catch (JSONException exception) {
            exception.printStackTrace();
        }
        return false;
    }

    private Callback<Void, PushwooshException> wrapCallback(CallbackContext callbackId, String errorLogMessage) {
        return new Callback<Void, PushwooshException>() {
            @Override
            public void process(@NonNull Result<Void, PushwooshException> result) {
                if (result.isSuccess()) {
                    callbackId.success();
                } else {
                    PWLog.error("PushwooshSecurePlugin", errorLogMessage, result.getException());
                    JSONObject exceptionObject = new JSONObject();
                    try {
                        exceptionObject.put("error", result.getException());
                        exceptionObject.put("errorDescription", result.getException().getLocalizedMessage());
                    }
                    catch (JSONException ignore) {
                    }
                    callbackId.error(exceptionObject);
                }
            }
        };
    }
}
