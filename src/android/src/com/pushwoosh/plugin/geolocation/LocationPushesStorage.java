package com.pushwoosh.plugin.geolocation;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.pddstudio.preferences.encrypted.EncryptedPreferences;
import com.pushwoosh.Pushwoosh;
import com.pushwoosh.internal.platform.AndroidPlatformModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

public class LocationPushesStorage {
    private static final String PREFERENCE_NAME = "PWBroadcastSecurityKey";
    private static final String PUSHES_KEY = "PUSHES_KEY";
    private static final String ENABLED_KEY = "ENABLED_KEY";

    private EncryptedPreferences encryptedPreferences;
    LocationPushesStorage() {
        encryptedPreferences = new EncryptedPreferences.Builder(AndroidPlatformModule.getApplicationContext())
                .withPreferenceName(PREFERENCE_NAME)
                .withEncryptionPassword(Pushwoosh.getInstance().getHwid())
                .build();
    }

    public void put(Bundle pushBundle, Location location) {
        synchronized (this) {
            try {
                JSONArray array = new JSONArray(encryptedPreferences.getString(PUSHES_KEY, "[]"));
                while (array.length() > 20) {
                    array.remove(0);
                }
                JSONObject pushWithLocation = new JSONObject();
                JSONObject push = new JSONObject();
                Set<String> keys = pushBundle.keySet();
                for (String key : keys) {
                    try {
                        push.put(key, JSONObject.wrap(pushBundle.get(key)));
                    } catch (JSONException skip) {
                    }
                }

                pushWithLocation.put("payload", push);
                JSONObject locationObject = new JSONObject();
                locationObject.put("latitude", location.getLatitude());
                locationObject.put("longitude", location.getLongitude());
                locationObject.put("accuracy", location.getAccuracy());

                pushWithLocation.put("geolocation", locationObject);

                array.put(pushWithLocation);

                encryptedPreferences.edit().putString(PUSHES_KEY, array.toString()).commit();
            } catch (JSONException ex) {
                ex.printStackTrace();
                clear();
            }
        }
    }

    public void clear() {
        encryptedPreferences.edit().remove(PUSHES_KEY).commit();
    }

    public JSONArray pop() {
        JSONArray array = null;
        synchronized (this) {
            try {
                array = new JSONArray(encryptedPreferences.getString(PUSHES_KEY, "[]"));
            } catch (JSONException ex) {
                ex.printStackTrace();
            } finally {
                clear();
            }
        }
        return array;
    }

    public void setLocationTrackingEnbaled(boolean enable) {
        encryptedPreferences.edit().putBoolean(ENABLED_KEY, enable).commit();
    }

    public boolean isLocationTrackingEnabled() {
        return encryptedPreferences.getBoolean(ENABLED_KEY, false);
    }
}
