/*
 *
 * Copyright (c) 2017. Pushwoosh Inc. (http://www.pushwoosh.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * (i) the original and/or modified Software should be used exclusively to work with Pushwoosh services,
 *
 * (ii) the above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.pushwoosh.plugin.geolocation;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.AndroidRuntimeException;

import com.google.android.gms.common.api.ResolvableApiException;
import com.pushwoosh.internal.crash.LogSender;
import com.pushwoosh.internal.event.Event;
import com.pushwoosh.internal.event.EventBus;
import com.pushwoosh.internal.utils.PWLog;

public class LocationSettingsResolutionActivity extends Activity {
    private static final String TAG = "LocationSettingsResolutionActivity";
    public static final String KEY_RESOLUTION = TAG + "KEY_RESOLUTION";
    public static final int REQUEST_CHECK_SETTINGS = 1;

    public static void resolutionSettingApi(Context context, ResolvableApiException exception) {
        PWLog.debug(TAG, "Request resolution");
        Intent intent = new Intent(context, LocationSettingsResolutionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(KEY_RESOLUTION, exception.getResolution());
        try {
            context.startActivity(intent);
        } catch (AndroidRuntimeException e) {
            LogSender.submitCustomError(e);
        }

    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PendingIntent resolution = getIntent().getParcelableExtra(KEY_RESOLUTION);
        if (resolution == null) {
            finish();
            return;
        }

        try {
            startIntentSenderForResult(resolution.getIntentSender(), REQUEST_CHECK_SETTINGS, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            PWLog.error("Can't start resolution for Resolution: " + resolution, e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        EventBus.sendEvent(new ResolutionEvent(true));
                        break;
                    case Activity.RESULT_CANCELED:
                        EventBus.sendEvent(new ResolutionEvent(false));
                        break;
                    default:
                        break;
                }
                break;
        }

        finish();
    }

    public static class ResolutionEvent implements Event {
        boolean success;

        ResolutionEvent(final boolean success) {
            this.success = success;
        }

        public boolean isSuccess() {
            return success;
        }
    }
}
