//
//  PushNotification.js
//
// Based on the Push Notifications Cordova Plugin by Olivier Louvignes.
// Modified by Pushwoosh team.
//
// Pushwoosh Push Notifications Plugin for Cordova
// www.pushwoosh.com
//
// MIT Licensed

var exec = require('cordova/exec');

//Class: PushwooshSecure
function PushwooshSecure() {}

PushwooshSecure.prototype.setBaseURL = function(options) {
	exec(null, null, "PushwooshSecure", "setBaseURL", [options]);
};

PushwooshSecure.prototype.setupDecryption = function(success, fail) {
    exec(success, fail, "PushwooshSecure", "setupDecryption", []);
}

PushwooshSecure.prototype.teardownDecryption = function(success, fail) {
    exec(success, fail, "PushwooshSecure", "teardownDecryption", []);
}
               
module.exports = new PushwooshSecure();