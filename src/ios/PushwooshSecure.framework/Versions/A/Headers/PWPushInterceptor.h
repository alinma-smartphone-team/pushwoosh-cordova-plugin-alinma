//
//  PWPushInterceptor.h
//  Secure Push
//  (c) Pushwoosh 2018
//

#import <Foundation/Foundation.h>

@protocol PWCryptoManager;

@protocol PWPushInterceptor <NSObject>

@required

- (BOOL)isValidForPayload:(NSDictionary *)payload;

/**
 *  @brief Transforms payload dictionary and returns modified version.
 */
- (void)transform:(NSDictionary *)payload withCompletion:(void(^)(NSDictionary *payload, NSError *error))completion;

@end
