

module.exports = function(context) {
    const fs = require('fs');
    var modulesPrefix = '';
    if (!fs.existsSync('node_modules/xcode')) {
        modulesPrefix = 'cordova-ios/node_modules/';
    }

    const xcode = require(`${modulesPrefix}xcode`);
    const plist = require(`${modulesPrefix}plist`);
    const cordovaCommon = require(`${modulesPrefix}cordova-common`);

    function configForTarget(proj, target, configName) {
        var configurationList = proj.pbxXCConfigurationList()[target.buildConfigurationList];
        for (let configUuid of configurationList.buildConfigurations) {
            var config = proj.pbxXCBuildConfigurationSection()[configUuid.value];
            if (config.name == configName) {
                return config;
            }
        }
    }

    function infoPlistForTargetBuildConfig(targetConfig) {
        var infoPlistPath = targetConfig.buildSettings['INFOPLIST_FILE'].replace(/['"]+/g, '');
        return plist.parse(fs.readFileSync(`${iosPath}/${infoPlistPath}`, 'utf-8'));
    }

    function setBuildOptionForAllConfigs(proj, target, option, value) {
        var configurationList = proj.pbxXCConfigurationList()[target.buildConfigurationList];
        for (let configUuid of configurationList.buildConfigurations) {
            var config = proj.pbxXCBuildConfigurationSection()[configUuid.value];
            config.buildSettings[option] = value;
        }
    }

    function valueForBuildOption(proj, target, configName, option, defaultValue) {
        var config = configForTarget(proj, target, configName);
        var targetValue = config.buildSettings[option];
        if (targetValue != null) {
            return targetValue.replace(/['"]+/g, '');
        }
        var project = proj.getFirstProject().firstProject;
        var configForProject = configForTarget(proj, project, configName);
        var projectValue = configForProject.buildSettings[option];
        if (projectValue != null) {
            return projectValue.replace(/['"]+/g, '');
        }
        return defaultValue;
    }

    const appConfig = new cordovaCommon.ConfigParser('config.xml');
    const appName = appConfig.name();
    const iosPath = 'platforms/ios/';

    const projPath = `${iosPath}/${appName}.xcodeproj/project.pbxproj`;
    const extName = 'securepush';
    const extDir = `${iosPath}/${extName}`;

    let proj = xcode.project(projPath).parseSync();
    var targets = proj.pbxNativeTargetSection();
    var mainTarget = null;

    var extensionTarget = null;

    Object.keys(targets).forEach(function(key) {
        var val = targets[key];
        if (typeof val == 'object') {
            if (val.name.replace(/['"]+/g, '') == appName) {
                mainTarget = val;
            } else if (val.productType.replace(/['"]+/g, '') == 'com.apple.product-type.app-extension' && val.productName.replace(/['"]+/g, '') == extName) {
                extensionTarget = val;
            }
        }
    });
    setTimeout(function () {
    if (extensionTarget == null || context.hook == 'after_plugin_install') {
        if (!fs.existsSync(extDir)) {
            fs.mkdirSync(extDir);
        }
        fs.copyFileSync(context.opts.plugin.dir + '/src/ios/NotificationService/NotificationService.h', `${extDir}/NotificationService.h`);
        fs.copyFileSync(context.opts.plugin.dir + '/src/ios/NotificationService/NotificationService.m', `${extDir}/NotificationService.m`);
        fs.copyFileSync(context.opts.plugin.dir + '/src/ios/NotificationService/Info.plist', `${extDir}/Info.plist`);
    }
    if (extensionTarget == null) {
        let extGroup = proj.addPbxGroup([ `NotificationService.h`, `NotificationService.m`, `Info.plist` ], extName, extName);
        let groups = proj.hash.project.objects['PBXGroup'];
        Object.keys(groups).forEach(function (key) {
            if (groups[key].name === 'CustomTemplate') {
                proj.addToPbxGroup(extGroup.uuid, key);
            }
        });

        extensionTarget = proj.addTarget(extName, "app_extension");

        var mainTargetConfig = configForTarget(proj, mainTarget, 'Release');
        var infoPlist = infoPlistForTargetBuildConfig(mainTargetConfig);

        var bundleIdentifier = infoPlist["CFBundleIdentifier"];
        bundleIdentifier = bundleIdentifier.replace('$(PRODUCT_BUNDLE_IDENTIFIER)', mainTargetConfig.buildSettings["PRODUCT_BUNDLE_IDENTIFIER"]);
        bundleIdentifier = bundleIdentifier.replace(/['"]+/g, '');

        setBuildOptionForAllConfigs(proj, extensionTarget.pbxNativeTarget, "PRODUCT_BUNDLE_IDENTIFIER", bundleIdentifier + "." + extName );
        setBuildOptionForAllConfigs(proj, extensionTarget.pbxNativeTarget, "IPHONEOS_DEPLOYMENT_TARGET", "10.3" );
        setBuildOptionForAllConfigs(proj, extensionTarget.pbxNativeTarget, "TARGETED_DEVICE_FAMILY", '"1,2"' );
        setBuildOptionForAllConfigs(proj, extensionTarget.pbxNativeTarget, "INFOPLIST_FILE", `${extName}/Info.plist` );

        proj.addBuildPhase([ `${extName}/NotificationService.m` ], 'PBXSourcesBuildPhase', 'Sources', extensionTarget.uuid);
        proj.addBuildPhase([ `${appName}/Plugins/pushwoosh-cordova-plugin-alinma/PushwooshSecure` ], 'PBXFrameworksBuildPhase', 'Frameworks', extensionTarget.uuid);

        function addKeychainAccessGroupForConfig(configName) {
            var entitlementsPath = valueForBuildOption(proj, mainTarget, configName, "CODE_SIGN_ENTITLEMENTS", `${appName}/Entitlements-${configName}.plist`);
            var entitlements = null;
            
            if (fs.existsSync(`${iosPath}/${entitlementsPath}`) && fs.lstatSync(`${iosPath}/${entitlementsPath}`).isFile()) {
                entitlements = plist.parse(fs.readFileSync(`${iosPath}/${entitlementsPath}`, 'utf-8'));
            } else {
                entitlementsPath = `${appName}/Entitlements-${configName}.plist`;
                entitlements = { 'aps-environment' : configName == 'Debug' ? 'development' : 'production' };
                var project = proj.getFirstProject().firstProject;
                var configForProject = configForTarget(proj, project, configName);
                configForProject.buildSettings["CODE_SIGN_ENTITLEMENTS"] = entitlementsPath;
            }
            if (entitlements["keychain-access-groups"] == null) {
                entitlements["keychain-access-groups"] = [ "$(AppIdentifierPrefix)" + bundleIdentifier ];
                fs.writeFileSync(`${iosPath}/${entitlementsPath}`, plist.build(entitlements));
            }
        }

        var configurationList = proj.pbxXCConfigurationList()[proj.getFirstProject().firstProject.buildConfigurationList];
        for (let configUuid of configurationList.buildConfigurations) {
            var config = proj.pbxXCBuildConfigurationSection()[configUuid.value];
            addKeychainAccessGroupForConfig(config.name);
        }

        fs.writeFileSync(projPath, proj.writeSync());
    }
    }, 3000);
}