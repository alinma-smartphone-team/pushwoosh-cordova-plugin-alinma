//
//  PWKeyManager.h
//  Secure Push
//  (c) Pushwoosh 2018
//

#import <Foundation/Foundation.h>

@interface PWKeyManager : NSObject

//Base URL for all requests and pin made from base64 encoded SHA256 of SPKI (for more details https://en.wikipedia.org/wiki/HTTP_Public_Key_Pinning )
+ (void)setBaseURLString:(NSString *)baseURLString pins:(NSArray<NSString *> *)pins overrideHost:(NSString *)overrideHost;

// Will generate keys if not exists
+ (void)setupDecryption:(void(^)(NSError *error))completion;
+ (void)teardownDecryption:(void(^)(NSError *error))completion;

@end
