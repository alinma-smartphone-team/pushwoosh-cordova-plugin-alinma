//
//  PushNotification.m
//
// Based on the Push Notifications Cordova Plugin by Olivier Louvignes on 06/05/12.
// Modified by Max Konev on 18/05/12.
//
// Pushwoosh Push Notifications Plugin for Cordova iOS
// www.pushwoosh.com
// (c) Pushwoosh 2012
//
// MIT Licensed

#import "PushwooshSecure.h"
#import "PWKeyManager.h"
#import "PWPushInterceptorProcessor.h"
#import "PushNotificationManager.h"
#import "PWLog.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation PushwooshSecure

#pragma clang diagnostic pop

- (void)setBaseURL:(CDVInvokedUrlCommand *)command {
    NSDictionary *params = [command.arguments firstObject];
    
    NSString *apiUrl = [params objectForKey:@"baseURL"];
    NSArray *pins = [params objectForKey:@"publicKeyPins"];
    NSString *overrideHost = [params objectForKey:@"overrideHost"];
 
    if ([UIDevice currentDevice].systemVersion.integerValue < 10) {
        NSLog(@"[PushwooshSecure] iOS 10+ required");
        return;
    }
    
    for (id obj in pins) {
        if (![obj isKindOfClass:[NSString class]]) {
            NSLog(@"[PushwooshSecure] pin should be base64 string");
            return;
        }
    }
    
    [PWKeyManager setBaseURLString:apiUrl pins:pins overrideHost:overrideHost];
}

- (void(^)(NSError *))wrapCallback:(NSString *)callbackId errorLogMessage:(NSString *)errorLogMessage {
    return ^(NSError *error) {
        if (error != nil) {
            PWLogError(@"%@: %@", errorLogMessage, error);
            [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:@{ @"error" : error.description, @"errorDescription" : error.localizedDescription }] callbackId:callbackId];
        } else {
            [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:nil] callbackId:callbackId];
        }
    };
}

- (void)setupDecryption:(CDVInvokedUrlCommand *)command {
    [PWKeyManager setupDecryption:[self wrapCallback:command.callbackId errorLogMessage:@"Error while setuping secure pushes"]];
}

- (void)teardownDecryption:(CDVInvokedUrlCommand *)command {
    [PWKeyManager teardownDecryption:[self wrapCallback:command.callbackId errorLogMessage:@"Error while tearing down secure pushes"]];
}

@end
