//
//  PWKeyExtractor.h
//  Secure Push
//  (c) Pushwoosh 2018
//

#import <Foundation/Foundation.h>

@interface PWSSecureStorage : NSObject

+ (void)saveObject:(id<NSCoding, NSObject>)object tag:(NSString *)tag error:(NSError **)error;
+ (id<NSCoding, NSObject>)loadObjectWithTag:(NSString *)tag error:(NSError **)error;
+ (void)deleteObjectWithTag:(NSString *)tag error:(NSError **)error;

@end
