package com.pushwoosh.plugin.geolocation;

import android.location.Location;
import android.os.Bundle;

import com.pushwoosh.internal.platform.AndroidPlatformModule;
import com.pushwoosh.notification.handlers.message.system.MessageSystemHandler;
import com.pushwoosh.notification.handlers.message.user.MessageHandler;

import java.util.Date;

public class LocationMessageHandler implements MessageSystemHandler {
    private LocationPushesStorage storage;
    LocationMessageHandler(LocationPushesStorage storage) {
        this.storage = storage;
    }
    @Override
    public boolean preHandleMessage(Bundle bundle) {
        String trackLocation = bundle.getString("trlc");
        if (trackLocation != null && trackLocation.equals("1") && storage.isLocationTrackingEnabled()) {
            new LocationManager().requestLocation(AndroidPlatformModule.getApplicationContext(), new Callback<Location>() {
                @Override
                public void call(Location result, Exception e) {
                    if (result != null && (result.getTime() + 600000) >= new Date().getTime()) {
                        storage.put(bundle, result);
                    }
                }
            });
        }
        return false;
    }
}
