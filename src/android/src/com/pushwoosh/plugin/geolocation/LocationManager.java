package com.pushwoosh.plugin.geolocation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.telecom.Call;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.pushwoosh.internal.event.EventBus;
import com.pushwoosh.internal.event.EventListener;
import com.pushwoosh.internal.event.PermissionEvent;
import com.pushwoosh.internal.utils.PermissionActivity;

public class LocationManager {
    public static final String[] LOCATION_PERMISSIONS = new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    private class ResolutionEventListener implements EventListener<LocationSettingsResolutionActivity.ResolutionEvent> {
        private Callback<Boolean> callback;
        public ResolutionEventListener(Callback<Boolean> callback) {
            this.callback = callback;
            EventBus.subscribe(LocationSettingsResolutionActivity.ResolutionEvent.class, this);
        }
        @Override
        public void onReceive(LocationSettingsResolutionActivity.ResolutionEvent resolutionEvent) {
            callback.call(resolutionEvent.success, null);
            EventBus.unsubscribe(LocationSettingsResolutionActivity.ResolutionEvent.class, this);
        }
    }

    private class PermissionsEventListener implements EventListener<PermissionEvent> {
        private Callback<Boolean> callback;
        public PermissionsEventListener(Callback<Boolean> callback) {
            this.callback = callback;
            EventBus.subscribe(PermissionEvent.class, this);
        }
        @Override
        public void onReceive(PermissionEvent resolutionEvent) {
            callback.call(resolutionEvent.getGrantedPermissions().size() > 0, null);
            EventBus.unsubscribe(PermissionEvent.class, this);
        }
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setNumUpdates(1);
        locationRequest.setInterval(10000);
        locationRequest.setMaxWaitTime(10000);
        locationRequest.setExpirationDuration(10000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    public void continueWithPermissionGranted(Context context, Callback<Boolean> callback) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(createLocationRequest());

        SettingsClient client = LocationServices.getSettingsClient(context);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                callback.call(true, null);
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    LocationSettingsResolutionActivity.resolutionSettingApi(context, (ResolvableApiException) e);
                    new ResolutionEventListener(callback);
                } else {
                    callback.call(false, e);
                }
            }
        });
    }

    public void requestPermissions(Context context, Callback<Boolean> callback) {
        boolean granted = false;
        for (String permission : LOCATION_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED) {
                granted = true;
            }
        }
        if (granted) {
            callback.call(true, null);
        } else {
            PermissionActivity.requestPermissions(context, LOCATION_PERMISSIONS);
            new PermissionsEventListener(new Callback<Boolean>() {
                @Override
                public void call(Boolean result, Exception e) {
                    if (result) {
                        continueWithPermissionGranted(context, callback);
                    } else {
                        callback.call(false, null);
                    }
                }
            });
        }
    }

    @SuppressLint("MissingPermission")
    public void requestLocation(Context context, Callback<Location> callback) {
        requestPermissions(context, new Callback<Boolean>() {
            @Override
            public void call(Boolean result, Exception e) {
                if (result == null || !result) {
                    callback.call(null, e);
                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(context);
                            client.requestLocationUpdates(createLocationRequest(), new LocationCallback() {
                                @Override
                                public void onLocationResult(LocationResult locationResult) {
                                    if (locationResult == null) {
                                        callback.call(null, null);
                                        return;
                                    }
                                    callback.call(locationResult.getLastLocation(), null);
                                    client.removeLocationUpdates(this);
                                }
                            },null);
                        }
                    });
                }
            }
        });
    }
}
